using Printf
using LinearAlgebra
using Plots
using HCubature

include("simulatedannealing.jl")

# using Flux
# import Flux.Tracker: TrackedVector, @grad, track
# using ForwardDiff: Dual, value, partials

eye(d) = Matrix{ComplexF64}(I, d, d)

id = λ0 = eye(3)
λ1 = ComplexF64[0 1 0; 1 0 0; 0 0 0]
λ3 = ComplexF64[1 0 0; 0 -1 0; 0 0 0]
λ4 = ComplexF64[0 0 1; 0 0 0; 1 0 0]
λ6 = ComplexF64[0 0 0; 0 0 1; 0 1 0]
λ8 = ComplexF64[1 0 0; 0 1 0; 0 0 -2] / sqrt(3)
λ = [λ1, λ3, λ4, λ6, λ8]

# Float64(x::Flux.Tracker.TrackedReal{Float64}) = Flux.data(x)

function one_body_op(op, i, n)
    ops = [id for i=1:n]
    ops[i] = op
    return reduce(kron, ops)
end

function two_body_op(opi, i, opj, j, n)
    ops = [id for i=1:n]
    ops[i] = opi
    ops[j] = opj
    return reduce(kron, ops)
end

function expectation(f, x, y; cutoff=1e-10, rtol=1e-2)

    # TODO how/why do we need to integrate???
    # functions are singular - go back and approximate the integrals
    # using some technique?
    
    # because we are evaluating an expectation value
    # this probably significantly slows the function?
    function fmod(p)
        px, py = p
        a, b = px*x, py*y
        res = f(a, b)
        if isnan(res)
            return 1.0
        else
            return res * exp(-(a^2+b^2))
        end
    end

    # p -> f(p[1]*x, p[2]*y)
    d = 10.0
    return hcubature(fmod, [-d, -d], [d, d]; rtol=rtol, maxevals=100000)[1]
end

function rewrite(x)
    re_ζa, re_ζb, im_ζa, im_ζb, Δa, Δb, γa, γb = x
    ζa = re_ζa + 1im * im_ζa
    ζb = re_ζb + 1im * im_ζb
    return ζa, ζb, Δa, Δb, γa, γb
end

function f1f(a, b)
    n = 3*a^2 * b^2 + (a^2 - b^2)^2 * cos(sqrt(a^2 + b^2)) + a^2 * b^2 * cos(2*sqrt(a^2 + b^2))
    d = (a^2 + b^2)^2
    return n/d
end

function f3f(a, b)
    n = sqrt(3) * (a^2 - b^2) * sin(sqrt(a^2 + b^2))^2
    d = 2*(a^2 + b^2)
    return n/d
end

function f4f(a, b)
    n = b^2 + a^2 * cos(sqrt(a^2 + b^2))
    d = a^2 + b^2
    return n/d
end

function f6f(a, b)
    n = a^2 + b^2 * cos(sqrt(a^2 + b^2))
    d = a^2 + b^2
    return n/d
end

function f8f(a, b)
    n = (1 + 3 * cos(2*sqrt(a^2 + b^2)))/4.0
    return n
end

function couplings(x, params)

    ζa, ζb, Δa, Δb, γa, γb = rewrite(x)

    s, ω, Ω, g = params

    # parametrizes squeezing
    A = ζ -> cosh(2*abs(ζ))
    function phase(ζ)
        return exp(1im*angle(ζ))
    end
    B = ζ -> phase(ζ)*sinh(2*abs(ζ))

    # TODO problem here is that the γ:s are Tracked Float64's
    # which forces integration to be over these Tracked variables
    # various functions are not defined for tracked variables!
    # ask someone in julia community about this?
    # According to HCubature, make code type-stable??

    # How about a function that takes a Tracked variable as input,
    # suspends the "tracking" somehow and then untracks it?
    f1a = expectation(f1f, γb, γa)
    f1b = expectation(f1f, γa, γb)
    f3a = expectation(f3f, γb, γa)
    f3b = expectation(f3f, γa, γb)
    f4a = expectation(f4f, γb, γa)
    f4b = expectation(f4f, γa, γb)
    f6a = expectation(f6f, γb, γa)
    f6b = expectation(f6f, γa, γb)
    f8a = expectation(f8f, γb, γa)
    f8b = expectation(f8f, γa, γb)

    # the neω parameter for the qutrit Hamiltonian,
    # basically staggered onsite fields and nearest-neighbor coupling
    # TODO check these formulas!
    re_AB_a = real(A(ζa) + B(ζa))
    re_AB_b = real(A(ζb) + B(ζb))
    abs_AB_a = abs(A(ζa) + B(ζa))
    abs_AB_b = abs(A(ζb) + B(ζb))

    h0a = ω*(abs(B(ζa))^2 + abs_AB_a^2 * (Δa^2 + 2*γa^2)) + g*2*re_AB_a*γa*(f4a+f6b)
    h0b = ω*(abs(B(ζb))^2 + abs_AB_b^2 * (Δb^2 + 2*γb^2)) + g*2*re_AB_b*γb*(f4b+f6a)
    ta = 2*g*re_AB_a*γa*(f4a+f6b)
    tb = 2*g*re_AB_b*γb*(f4b+f6a)
    ca1 = s*f1a
    cb1 = s*f1b
    ca3 = -Ω*f3a
    cb3 = -Ω*f3b
    ca4 = 2*g*re_AB_a*Δa*f4a
    cb4 = 2*g*re_AB_b*Δb*f4b
    ca6 = 2*g*re_AB_b*Δb*f6a
    cb6 = 2*g*re_AB_a*Δa*f6b
    ca8 = -Ω*f8a
    cb8 = -Ω*f8b

    return ta, tb, (ca1, ca3, ca4, ca6, ca8), (cb1, cb3, cb4, cb6, cb8)
end

function hamiltonian(x, params, n_cells)

    ta, tb, ca, cb = couplings(x, params)

    ua = sum([ci*λi for (ci, λi) in zip(ca, λ)])
    ub = sum([ci*λi for (ci, λi) in zip(cb, λ)])

    n = 2 * n_cells
    D = 3^n
    H = zeros(ComplexF64, D, D)
    for k=1:n_cells
        H += one_body_op(ua, 2*k-1, n)
        H += one_body_op(ub, 2*k, n)
    end
    for k=1:(n_cells-1)
        H += two_body_op(ta*λ4, 2*k-1, λ6, 2*k, n)
        H += two_body_op(tb*λ4, 2*k, λ6, 2*k+1, n)
    end
    return H
end

n_cells = 2

params_sf = s, ω, Ω, g = -2.0, 1.0, 1.0, 1.0
params_peierls = s, ω, Ω, g = 0.0, 1.0, 1.0, 0.6
params_y = s, ω, Ω, g = -0.01, 1.0, 1.0, 1.5
params_normal = s, ω, Ω, g = -2.0, 1.0, 1.0, 0.2

function e0(x, params, n_cells)
    H = hamiltonian(x, params, n_cells)
    return minimum(eigvals(H))
end

function callback(x)
    x = rewrite(x)
    println("ζ=$(abs.(x[1:2])), Δ=$(x[3:4]), γ=$(x[5:6])")
end

# callback(x) = nothing

function rand_uniform(a, b)
    a + rand() * (b - a)
end

neighbor(x) = [rand_uniform(x[i] - 1.0, x[i] + 1.0) for i=1:length(x)]

x = rand(Float64, 8)

# Try simulated annealing first!
x = simulated_annealing(x -> e0(x, params_sf, n_cells), x, neighbor;
                        trace=false, callback=callback,
                        temperature=(i -> 1), iterations=100000)

callback(x)

# e0(x::TrackedVector, params, n_cells) = track(e0, x, params, n_cells)
#
# @grad function e0(x, params, n_cells)
#   s = e0(Tracker.data(x) .+ [Dual(0,1,0,0,0,0,0,0,0),
#                              Dual(0,0,1,0,0,0,0,0,0),
#                              Dual(0,0,0,1,0,0,0,0,0),
#                              Dual(0,0,0,0,1,0,0,0,0),
#                              Dual(0,0,0,0,0,1,0,0,0),
#                              Dual(0,0,0,0,0,0,1,0,0),
#                              Dual(0,0,0,0,0,0,0,1,0),
#                              Dual(0,0,0,0,0,0,0,0,1)], params, n_cells)
#   value(s), Δ -> (Δ .* partials(s), nothing)
# end
#
# x = param(rand(Float64, 8))
#
# data = Iterators.repeated((), 100)
#
# # ADAM(params, η = 0.001; β1 = 0.9, β2 = 0.999, ϵ = 1e-08, decay = 0)
#
# opt = ADAM([x], 0.1)
#
# function cb()
#     println(rewrite(Flux.data(x)))
#     # TODO save CSV file?
# end
#
# cb()
#
# Flux.train!(() -> e0(x, params_peierls, n_cells), data, opt, cb = cb)
# cb()
