import numpy as np
from scipy.sparse.linalg import eigsh
from scipy.sparse.linalg.eigen.arpack.arpack import ArpackError
import scipy.optimize as opt
from scipy.integrate import dblquad
from represent.represent.sitetypes import SiteType
from represent.represent.operators import Identity, MatrixOperator
from represent.represent.lattice import unit_cell_chain
from represent.represent.hamiltonian import Hamiltonian
from represent.represent.exact import exact_hamiltonian


def load_or_create_datafile(label):
    x0 = np.zeros(8)
    try:
        x0 = np.loadtxt("mf_params_%s.dat" % label)[:-1]
    except OSError:
        np.savetxt("mf_params_%s.dat" % label, np.hstack((x0, 100)))
    return x0


def save_mf_params(label, mf_params, e0):
    try:
        e = np.loadtxt("mf_params_%s.dat" % label)[-1]
        if e0 < e:
            np.savetxt("mf_params_%s.dat" % label, np.hstack((mf_params, e0)))
    except OSError:
        np.savetxt("mf_params_%s.dat" % label, np.hstack((mf_params, e0)))


def rewrite_mf(mf_params):
    zeta_a_re, zeta_b_re, zeta_a_imag, zeta_b_imag, delta_a, delta_b, gamma_a, gamma_b = mf_params
    zeta_a = zeta_a_re + 1j* zeta_a_imag
    zeta_b = zeta_b_re + 1j* zeta_b_imag
    return zeta_a, zeta_b, delta_a, delta_b, gamma_a, gamma_b


def print_mf_params(mf_params, e):
    zeta_a, zeta_b, delta_a, delta_b, gamma_a, gamma_b = rewrite_mf(mf_params)
    print("gamma=%0.3f,%0.3f, zeta=%0.3f,%0.3f, delta=%0.3f,%0.3f, E/site=%0.10f" %
      (gamma_a, gamma_b,
       np.abs(zeta_a), np.abs(zeta_b),
       delta_a, delta_b,
       e))


def expectation(f, x, y, cutoff=1e-10, epsrel=1e-6):
    # this probably significantly slows the function?
    def fmod(px, py):
        res = f(px, py, x, y)
        if np.isnan(res):
            return 1.0
        else:
            return f(px, py, x, y)*np.exp(-(px**2+py**2))

    # def fmod(px, py):
    #     if (x*px)**2+(y*py)**2 > cutoff**2:
    #         return f(px, py, x, y)*np.exp(-px**2-py**2)
    #     else:
    #         return 1
    d = np.inf
    return dblquad(fmod, -d, d, -d, d, epsrel=epsrel)[0]


def qutrit_hamiltonian(params):
    h0a, h0b, h1a, h1b, h3a, h3b, h4a, h4b, h6a, h6b, h8a, h8b, h46a, h46b = params
    # TODO create utility to link the site type to chain
    qutrit = SiteType("qutrit", 3, 1)
    types = {'a': qutrit, 'b': qutrit}
    l1 = MatrixOperator(qutrit, "L1", np.array([[0, 1, 0], [1, 0, 0], [0, 0, 0]]))
    l3 = MatrixOperator(qutrit, "L3", np.array([[1, 0, 0], [0, -1, 0], [0, 0, 0]]))
    l4 = MatrixOperator(qutrit, "L4", np.array([[0, 0, 1], [0, 0, 0], [1, 0, 0]]))
    l6 = MatrixOperator(qutrit, "L6", np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0]]))
    l8 = MatrixOperator(qutrit, "L8", np.array([[1, 0, 0], [0, 1, 0], [0, 0, -2]]))
    i = Identity(qutrit)
    t0a = h0a, [i]
    t1a = h1a, [l1]
    t3a = h3a, [l3]
    t4a = h4a, [l4]
    t6a = h6a, [l6]
    t8a = h8a, [l8]
    t0b = h0b, [i]
    t1b = h1b, [l1]
    t3b = h3b, [l3]
    t4b = h4b, [l4]
    t6b = h6b, [l6]
    t8b = h8b, [l8]
    t46a = h46a, [l4, l6]
    t46b = h46b, [l4, l6]
    terms = {'onsite_a': [t0a, t1a, t3a, t4a, t6a, t8a],
             'onsite_b': [t0b, t1b, t3b, t4b, t6b, t8b],
             'nn_a': [t46a],
             'nn_b': [t46b]}
    return Hamiltonian(types, terms)

# the expectation values for the non-adiabatic polaron transformation
# TODO check these against Mathematica
def f1f(px, py, x, y):
    a = px*x
    b = py*y
    n = 3*a**2 * b**2 + (a**2 - b**2)**2 * np.cos(np.sqrt(a**2 + b**2)) + \
        a**2 * b**2 * np.cos(2*np.sqrt(a**2 + b**2))
    d = (a**2 + b**2)**2
    return n/d


def f3f(px, py, x, y):
    a = px*x
    b = py*y
    n = np.sqrt(3) * (a**2 - b**2) * np.sin(np.sqrt(a**2 + b**2))**2
    d = 2.*(a**2 + b**2)
    return n/d


def f4f(px, py, x, y):
    a = px*x
    b = py*y
    n = b**2 + a**2 * np.cos(np.sqrt(a**2 + b**2))
    d = a**2 + b**2
    return n/d


def f6f(px, py, x, y):
    a = px*x
    b = py*y
    n = a**2 + b**2 * np.cos(np.sqrt(a**2 + b**2))
    d = a**2 + b**2
    return n/d


def f8f(px, py, x, y):
    a = px*x
    b = py*y
    n = (1 + 3 * np.cos(2*np.sqrt(a**2 + b**2)))/4.
    return n

def variational_qutrit_hamiltonian(mf_params, params):

    zeta_a, zeta_b, delta_a, delta_b, gamma_a, gamma_b = rewrite_mf(mf_params)
    s, w, O, g = params

    # parametrizes squeezing
    A = lambda zeta: np.cosh(2*np.abs(zeta))
    def phase(zeta):
        return np.exp(1j*np.angle(zeta))
    B = lambda zeta: np.exp(1j*np.angle(zeta))*np.sinh(2*np.abs(zeta))

    f1a = expectation(f1f, gamma_b, gamma_a)
    f1b = expectation(f1f, gamma_a, gamma_b)
    f3a = expectation(f3f, gamma_b, gamma_a)
    f3b = expectation(f3f, gamma_a, gamma_b)
    f4a = expectation(f4f, gamma_b, gamma_a)
    f4b = expectation(f4f, gamma_a, gamma_b)
    f6a = expectation(f6f, gamma_b, gamma_a)
    f6b = expectation(f6f, gamma_a, gamma_b)
    f8a = expectation(f8f, gamma_b, gamma_a)
    f8b = expectation(f8f, gamma_a, gamma_b)

    print([f1a, f1b, f3a, f3b, f4a, f4b, f6a, f6b, f8a, f8b])

    # the new parameter for the qutrit Hamiltonian,
    # basically staggered onsite fields and nearest-neighbor coupling
    # TODO check these formulas!
    re_AB_a = np.real(A(zeta_a) + B(zeta_a))
    re_AB_b = np.real(A(zeta_b) + B(zeta_b))
    abs_AB_a = np.abs(A(zeta_a) + B(zeta_a))
    abs_AB_b = np.abs(A(zeta_b) + B(zeta_b))

    h0a = w*(np.abs(B(zeta_a))**2 + \
          abs_AB_a**2 * (delta_a**2 + 2*gamma_a**2)) + \
          g*2*re_AB_a*gamma_a*(f4a+f6b)
    h0b = w*(np.abs(B(zeta_b))**2 + \
          abs_AB_b**2 * (delta_b**2 + 2*gamma_b**2)) + \
          g*2*re_AB_b*gamma_b*(f4b+f6a)
    h1a = s*f1a
    h1b = s*f1b
    h3a = -O*f3a
    h3b = -O*f3b
    h4a = 2*g*re_AB_a*delta_a*f4a
    h4b = 2*g*re_AB_b*delta_b*f4b
    h6a = 2*g*re_AB_b*delta_b*f6a
    h6b = 2*g*re_AB_a*delta_a*f6b
    h8a = -O*f8a
    h8b = -O*f8b
    h46a = 2*g*re_AB_a*gamma_a*(f4a+f6b)
    h46b = 2*g*re_AB_b*gamma_b*(f4b+f6a)

    new_params = h0a, h0b, h1a, h1b, h3a, h3b, h4a, h4b, h6a, h6b, h8a, h8b, h46a, h46b

    return qutrit_hamiltonian(new_params)


def diagonalize_params(label, mf_params, params, N_cells, periodic=False):
    cell = ['a', 'b']
    lattice = unit_cell_chain(cell, N_cells, periodic=periodic)
    h = variational_qutrit_hamiltonian(mf_params, params)
    H = exact_hamiltonian(h, lattice)
    # TODO sparse!!!
    # TODO take expectation values!
    try:
        e, v = eigsh(H, k=1, which='SA')
        e_per_site = e[0]/(2*N_cells)
        print_mf_params(mf_params, e_per_site)
        assert(not np.isnan(e_per_site))
        save_mf_params(label, mf_params, e_per_site)
        return e_per_site
    except (ArpackError, AssertionError):
        print("eigsh error")
        return 100


def minimize_mf(label, params, N_cells, periodic=False, x0=[0,0,0,0,0,0,0,0],
                method='anneal', simplifier=False):

    def print_fun(x, f, accepted):
        print("at minima %.4f: accepted?: %d" % (f, accepted))

    def f(x):
        if simplifier != False:
            x = simplifier(x)
        return diagonalize_params(label, x, params, N_cells, periodic=periodic)

    if method == 'anneal':
        return opt.basinhopping(f, x0,
                                minimizer_kwargs={'method': 'powell'},
                                callback=print_fun,
                                T=10,
                                niter=100)
    elif method == 'minimize':
        return opt.minimize(f, x0)
    else:
        raise NotImplementedError

# TODO compare with ED or DMRG of full model?
# Does GS energy come close?

# # unit cell 1
# label = "superfluid"
# N_cells = 2
# params = s, w, O, g = -2.0, 1.0, 1.0, 1.0
# x0 = load_or_create_datafile(label)
# x0 = x0[0::2]
# res = minimize_mf(label, params, N_cells, periodic=True, x0=x0,
#                   method='anneal', simplifier=lambda x: np.repeat(x, 2))

# unit cell 2
# label = "superfluid_unit_cell_2"
# N_cells = 2
# params = s, w, O, g = -2.0, 1.0, 1.0, 2.0
# x0 = load_or_create_datafile(label)
# res = minimize_mf(label, params, N_cells, periodic=True, x0=x0, method='anneal')

label = "peierls"
N_cells = 3
params = s, w, O, g = -0.05, 1.0, 1.0, 0.5
x0 = load_or_create_datafile(label)
res = minimize_mf(label, params, N_cells, periodic=True, x0=x0, method='anneal')

# label = "y"
# N_cells = 2
# params = s, w, O, g = -0.05, 1.0, 1.0, 1.5
# x0 = load_or_create_datafile(label)
# res = minimize_mf(label, params, N_cells, periodic=True, x0=x0, method='anneal')

# label = "normal"
# N_cells = 2
# params = s, w, O, g = -2.0, 1.0, 1.0, 0.2
# x0 = load_or_create_datafile(label)
# res = minimize_mf(label, params, N_cells, periodic=True, x0=x0, method='anneal')

# TODO do a sweep between normal and superfluid phases?
