using Printf, LinearAlgebra, Plots, HCubature

using Flux
import Flux.Tracker: TrackedVector, @grad, track
using ForwardDiff: Dual, value, partials

f(x) = hcubature(y -> prod(sin.(x .* y)), [0, 0], [π, π])[1]
f(x::TrackedVector) = track(f, x)

@grad function f(x)
  s = f(Tracker.data(x) .+ [Dual(0,1,0), Dual(0,0,1)])
  value(s), Δ -> (Δ .* partials(s), nothing)
end

x = param(rand(Float64, 2))
data = Iterators.repeated((), 100)
opt = ADAM([x], 0.1)

function cb()
    println(Flux.data(x))
end

Flux.train!(() -> f(x), data, opt, cb = cb)
